import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class MyORM {

    public static Session session;
    public static void insert(final PointEntity row) {
        session = MyHibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.save(row);
        session.getTransaction().commit();
        session.close();
    }

    public static List<PointEntity> getRows () {
        session = MyHibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        Query query = session.createQuery("from PointEntity");
        final List<PointEntity> results = query.list();
        session.getTransaction().commit();
        session.close();
        return results;
    }

    public static void clear () {
        session = MyHibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        Query query = session.createQuery("delete from PointEntity");
        query.executeUpdate();
        session.getTransaction().commit();
        session.close();
    }

}