import javax.persistence.*;


import org.hibernate.annotations.OptimisticLockType;
@Entity
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.ALL, dynamicUpdate = true)
public class PointEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id",unique = true, nullable = false)
    private int id;
    @Column(name = "x",nullable = false)
    private double x;
    @Column(name = "y",nullable = false)
    private double y;
    @Column(name = "r",nullable = false)
    private double r;
    @Column(name = "hit",nullable = false)
    private String hit;
    public PointEntity(){}
    public PointEntity(double x, double y, double r){
        this.x = x;
        this.y = y;
        this.r = r;
        hit = check();
    }

    private String check(){
        if(x<=r && x >= 0 && y <= 0 && y >= -1*r){
            return "Rectangle";
        }
        if((x<=0) && (y>=0) && (y<=(2*x+r))){
            return "Triangle";
        }
        if(x>=0 && y>=0 && (x*x+y*y<=r*r/4)){
            return "Circle";
        }
        return "Void";
    }
    public String getHit() {
        return hit;
    }

    public void setHit(String hit) {
        this.hit = hit;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void setR(double r) {
        this.r = r;
    }

    public double getY() {
        return y;
    }

    public double getR() {
        return r;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getX() {
        return x;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
