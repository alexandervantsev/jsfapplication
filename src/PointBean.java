import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@ManagedBean(name = "points")
@SessionScoped
public class PointBean implements Serializable{
private double x=0.0, y=0.0, r=1.0;
private boolean hit;
private List<PointEntity> pointsList = new ArrayList<>();


    public void add(){
        final PointEntity pe = new PointEntity(this.x,this.y,this.r);
        pointsList.add(pe);
        MyORM.insert(pe);
    }

    public List<PointEntity> getPointsList() {
        pointsList = MyORM.getRows();
        return pointsList;

    }

    public void setPointsList(List<PointEntity> pointsList) {
        this.pointsList = pointsList;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getR() {
        return r;
    }

    public double getY() {
        return y;
    }

    public void setR(double r) {
        this.r = r;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void setHit(boolean hit) {
        this.hit = hit;
    }

    public boolean isHit() {
        return hit;
    }
}

