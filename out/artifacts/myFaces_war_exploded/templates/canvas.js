var y,r;
var scale = 80;
var canvas=document.getElementById("myCanvas");
var context=canvas.getContext("2d");
function drawCanvas() {
    getR();

    context.clearRect(0,0,canvas.width,canvas.height);
    drawGraph(context);
    drawCoordinateLines(context);
    drawPoints();
}

function drawCoordinateLines(context) {
    context.beginPath();
    context.moveTo(300,600);
    context.lineTo(300,0);
    context.lineTo(305,5);
    context.moveTo(300,0);
    context.lineTo(295,5);
    context.moveTo(0,300);
    context.lineTo(600,300);
    context.lineTo(595,305);
    context.moveTo(600,300);
    context.lineTo(595,295);

    context.strokeStyle="black";
    context.stroke();
    context.fillStyle="black";
    context.closePath();
}
function drawGraph(context){
    context.beginPath();
    //0.25 circle
    context.arc(301,299,r/2*scale,1.5*Math.PI,2*Math.PI);
    context.lineTo(301,299);
    context.moveTo((301 - r/2 *scale), 301);
    context.lineTo(301,(301 + r/2*scale));
    //triangle
    context.moveTo(299-r/2*scale, 299);
    context.lineTo(299, 299);
    context.lineTo(299, 299 - r*scale);
    context.lineTo(299-r/2*scale, 299);
    //rectangle
    context.rect(301,301, r*scale, r*scale );

    context.closePath();
    context.fillStyle="lightBlue";
    context.fill();
}
drawCanvas();
function drawPoint(x, y, hit) {
    var canvas=document.getElementById("myCanvas");
    var context=canvas.getContext("2d");
    context.beginPath();
    if (hit) {
        context.fillStyle = "Green";
    } else {
        context.fillStyle = "Red";
    }
    context.arc(x, y, 3, 0 * Math.PI, 2 * Math.PI);
    context.fill();

}
function drawPoints() {
    var _x, _y, _r, _hit;
    var str = document.getElementById("results").outerHTML;

    if ((str.indexOf("<td></td>")===-1) &&(str !== null)){
        while (str.indexOf("<tr>")!==-1){
            _x = parseFloat(str.substring(str.indexOf("<td>")+4,str.indexOf("</td>")));
            str = str.substring(str.indexOf("</td>")+5);
            _y = parseFloat(str.substring(str.indexOf("<td>")+4,str.indexOf("</td>")));
            str = str.substring(str.indexOf("</td>")+5);
            str = str.substring(str.indexOf("</td>")+5);
            str = str.substring(str.indexOf("</td>")+5);
            _hit = getHit(_x,_y,r);
            _y = _y*scale*(-1) + 300;
            _x = _x*scale + 300;
            drawPoint(_x,_y,_hit);
            console.log(_hit);
        }
    }
}




